# RPI Ubuntu

## Before runing script

Create user
``` bash
# Create a user and make it administrator
sudo adduser [name]
sudo usermod -G sudo [name]
```

Create a wifi netplan /etc/netplan/configure-wifi.yaml
``` yaml
network:
  version: 2
  wifis:
    wlan0:
      dhcp4: true
      access-points:
        "[SSID]":
          password: "[password]"
```

Generate and apply the netplan
``` bash
sudo netplan generate
sudo netplan apply
```
