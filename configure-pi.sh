#!/bin/sh

USER_NAME='ubuntu'
USER_STATUS=$(passwd -Sa | awk -v UN=$USER_NAME ' $1 ~ UN { print $2 }')

if test "$USER_STATUS" = 'P'; then
  passwd -l $USER_NAME
  echo "$USER_NAME locked"
fi

if test -n "$(id $USER_NAME)"; then
  deluser --remove-home $USER_NAME
fi
# Test that the user is deleted with the commands
# $ id ubuntu
# $ grep '^ubuntu' /etc/passwd

unset USER_NAME
unset USER_STATUS
